import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplinceComponent } from './complince.component';

describe('ComplinceComponent', () => {
  let component: ComplinceComponent;
  let fixture: ComponentFixture<ComplinceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplinceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplinceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { DrawComponent } from './pages/draw/draw.component';
import { LoanlistingComponent } from './pages/loanlisting/loanlisting.component';
import { HeaderComponent } from './pages/header/header.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ComplianceComponent } from './pages/compliance/compliance.component';
import { FundingComponent } from './pages/funding/funding.component';
import { RatingComponent } from './pages/rating/rating.component';
import { FooterComponent } from './pages/footer/footer.component';


const routes: Routes = [

  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'draw', component: DrawComponent },
  { path: 'loanlisting', component: LoanlistingComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'compliance', component: ComplianceComponent },
  { path: 'funding', component: FundingComponent },
  { path: 'rating', component: RatingComponent },
  { path: 'footer', component: FooterComponent },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
    })
  ],  exports: [RouterModule]
})
export class AppRoutingModule { }

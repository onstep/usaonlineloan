import { Component, OnInit, AfterViewChecked, HostListener  } from '@angular/core';
import * as $ from 'jquery';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @ViewChild('pageMenu')
  menuStickyElement;
  offsetTop: number;
  stickyNavMenu: boolean;
  constructor() { }

  ngOnInit() {
  }
  @HostListener('window:scroll', [])
  sticklyMenu() {
    // Get top height 
    this.offsetTop = document.documentElement.scrollTop;
    // if (document.documentElement.scrollTop >= this.menuStickyElement.nativeElement.offsetTop) {
    //   console.log(document.documentElement.scrollTop);
    //   console.log(this.menuStickyElement.nativeElement.offsetTop);

    //   this.stickyNavMenu = true;
    //   console.log('white');
    // } else {
    //   console.log(this.menuStickyElement.nativeElement.offsetTop);

    //   this.stickyNavMenu = false;
    //   console.log('trasparent');
    // }
    if(document.documentElement.scrollTop==0){
      console.log('white');
      this.stickyNavMenu = false;

    }else{
      console.log('trans');
      this.stickyNavMenu = true;
    }
  }
}

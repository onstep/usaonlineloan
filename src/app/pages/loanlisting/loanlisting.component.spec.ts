import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanlistingComponent } from './loanlisting.component';

describe('LoanlistingComponent', () => {
  let component: LoanlistingComponent;
  let fixture: ComponentFixture<LoanlistingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanlistingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanlistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

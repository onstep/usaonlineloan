import { Component, OnInit, AfterViewChecked, HostListener  } from '@angular/core';
import * as $ from 'jquery';
import { ViewChild } from '@angular/core';

import {
    TweenMax,
    TimelineLite,
    TimelineMax,
    Linear
} from 'gsap';
// import ScrollMagic from 'scrollmagic';
import * as ScrollMagic from 'scrollmagic'; // Or use scrollmagic-with-ssr to avoid server rendering problems
// import { TweenMax, TimelineMax } from "gsap"; // Also works with TweenLite and TimelineLite
import { ScrollMagicPluginGsap } from 'scrollmagic-plugin-gsap';
// import 'imports-loader?define=>false!scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js';

import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax);

// import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
// import 'gsap';
// import * as DrawSVGPlugin from 'gsap';// i can comment that out, same behaviour -.-
// import 'imports-loader?define=>false!animation.gsap'; // needed due to bug in ScrollMagic
// import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
@HostListener('window:scroll', ['$event'])
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  ctrl = new ScrollMagic.Controller();
  svg;
//   ngOnInit() {
// console.log('dffh');
//     // new ScrollMagic.Scene({
//     //   triggerElement: '.menu-trigger',
//     //   duration: window.screen.height,
//     //   triggerHook: 0
//     // })
//     // .setPin('#menu', { pushFollowers: false })
//     // .addTo(this.ctrl);


// function pathPrepare($el) {
//  // alert();
//       const lineLength = $el[0].getTotalLength();
//       $el.css('stroke-dasharray', lineLength);
//       $el.css('stroke-dashoffset', lineLength);
//     }

// const $word = $('path#word');
// const $dot = $('path#dot');

//     // prepare SVG
// pathPrepare($word);
// pathPrepare($dot);

//     // init controller
// const controller = new ScrollMagic.Controller();
// console.log('ds');
//     // build tween
// const tween = new TimelineMax()
//       .add(TweenMax.to($word, 0.9, {strokeDashoffset: 0, ease: Linear.easeNone})) // draw word for 0.9
//       .add(TweenMax.to($dot, 0.1, {strokeDashoffset: 0, ease: Linear.easeNone}))  // draw dot for 0.1
//       .add(TweenMax.to('path', 1, {stroke: '#33629c', ease: Linear.easeNone}), 0);			// change color during the whole thing
// console.log('dss');

//     // build scene
// const scene = new ScrollMagic.Scene({triggerElement: '#trigger1', duration: 200, tweenChanges: true})
//             .setTween(tween)
//             .addIndicators() // add indicators (requires plugin)
//             .addTo(controller);

//   }
  ngOnInit() {
    function pathPrepare($el) {
      // tslint:disable-next-line: prefer-const
   //   var path = document.querySelector('path#word');
    //  let path = <HTMLElement>document.querySelector('path#word');
    const myPath = document.querySelector('path#word');
    console.log(myPath);

 //   var length = myPath.getTotalLength();
   // var length = myPath.getTotalLength();
    var length = 1748.7900390625;
    console.log('length');
    console.log(length);

// tslint:disable-next-line: no-trailing-whitespace
      console.log(length); 
      // const lineLength = $el[0].getTotalLength();
      const lineLength = length;
      $el.css('stroke-dasharray', lineLength); 
      $el.css('stroke-dashoffset', lineLength);

    }
    firstDraw();
    secondDraw();

    function firstDraw() {
      const duration = $('.applyLoan').height();
      const $word = $('path#word');

        // prepare SVG
      pathPrepare($word);
        // pathPrepare($dot);

        // init controller
      const controller = new ScrollMagic.Controller();

        // build tween
      const tween = new TimelineMax()
          .add(TweenMax.to($word, 2.0, {strokeDashoffset: 0, ease: Linear.easeNone})) // draw word for 0.9
         // .add(TweenMax.to('path', 1, {ease: Linear.easeNone}), 0);			// change color during the whole thing

        // build scene
      const scene = new ScrollMagic.Scene({triggerElement: '#trigger1', duration, tweenChanges: true})
                .setTween(tween)
               //  .addIndicators() // add indicators (requires plugin)
                .addTo(controller);
    }

    function secondDraw() {
      // tslint:disable-next-line: prefer-const
     
        const duration = $('.chooseLoan').height();
        const $word = $('path#word2');
      //  console.log(isMobile, 111);
          // prepare SVG
        pathPrepare($word);
          // pathPrepare($dot);

          // init controller
        const controller = new ScrollMagic.Controller();

          // build tween
        const tween = new TimelineMax()
            .add(TweenMax.to($word, 0.9, {strokeDashoffset: 0, ease: Linear.easeNone})) // draw word for 0.9
            .add(TweenMax.to('path', 1, {ease: Linear.easeNone}), 0);			// change color during the whole thing

          // build scene
        const scene = new ScrollMagic.Scene({triggerElement: '#trigger1', duration: duration - 200, tweenChanges: true})
                  .setTween(tween)
                // .addIndicators() // add indicators (requires plugin)
                  .addTo(controller);
      



    }
  }
  // onWindowScroll($event) {
  //   let element = document.querySelector('#navbar');
  //   if (window.pageYOffset > 100) {
  //     element.classList.add('.header-transparent');
  //   } else {
  //     element.classList.remove('.top-nav-collapse');
  //   }
  // }
}

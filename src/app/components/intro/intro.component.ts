import { Component, OnInit } from '@angular/core';
import {coerceNumberProperty} from '@angular/cdk/coercion';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {
  clicked='1';
  rocket = {
    path: 'https://assets6.lottiefiles.com/datafiles/D2dX6GVqV6yrIAT/data.json',
    renderer: 'canvas',
    autoplay: true,
    loop: true
};
selected;
otp='no';
autoTicks = false;
disabled = false;
invert = false;
max = 10000;
min = 200;
showTicks = false;
step = 1;
thumbLabel = false;
value = 200;
vertical = false;


get tickInterval(): number | 'auto' {
  return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
}
set tickInterval(value) {
  this._tickInterval = coerceNumberProperty(value);
}
// tslint:disable-next-line: variable-name
private _tickInterval = 200;

  constructor() { }

  ngOnInit() {
  //  this.selectit('gg');
  }

  selectit(type){
    // this.clicked='0';

    // this.otp='getotp';
    
    // return;
    this.selected=type;
    this.clicked='0';
    console.log(this.selected);
  }
  back(){
    this.clicked='1';
  }
}
